class DBToto {
  int id;
  String title;
  String description;

  DBToto({
    this.id = 0,
    this.title ='',
    this.description ='',
  });

  factory DBToto.fromMap(Map<String, dynamic> json) => DBToto(
    id: json['id'] ?? 0,
      title: json['title'] ?? '',
      description: json['description'] ?? '',
  );
  Map<String, dynamic> toMap() => {
    "id": id,
    "title": title,
  "description": description,
  };
}