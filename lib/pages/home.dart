import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {

  late String _userToDo;
  List todolist = [];

  @override
  void initState() {
    super.initState();

    todolist.addAll(['Buy milk', 'Wash dishes', 'byu mark']);
  }

  void _menuOpen() {
    Navigator.of(context).push(
      MaterialPageRoute (builder: (BuildContext context) {
        return Scaffold(
            appBar: AppBar(title: Text ('Menu') ),
            body: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column (
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      ElevatedButton(onPressed: () {
                        Navigator.pop(context);
                        Navigator.pushNamedAndRemoveUntil(context, '/', (route) => false);
                      }, child: Text ('На головну')),
                      Padding(padding: EdgeInsets.only(left: 15)),
                      Text ('Наше просте меню')
                    ],
                  )
                ]
            )
        );
      }),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[500],
      appBar: AppBar(
        title: Text('Список справ на сьогодні'),
        centerTitle: true,
        actions: [
          IconButton(onPressed: _menuOpen, icon: Icon(Icons.menu_outlined))
        ],
      ),
      body: ListView.builder(
          itemCount: todolist.length,
          itemBuilder: (BuildContext context, int index) {
            return Dismissible(
                key: Key(todolist[index]),
                child: Card(
                  child: ListTile(
                    title: Text(todolist[index]),
                    trailing: IconButton(
                      icon: Icon(
                        Icons.delete,
                        color: Colors.lightBlueAccent,
                      ),
                      onPressed: () {
                        setState(() {
                          todolist.removeAt(index);
                        });
                      },
                    ),
                  ),
                ),
                onDismissed: (direction) {
                  todolist.removeAt(index);
                });
          }
      ),
      floatingActionButton: FloatingActionButton(
          backgroundColor: Colors.greenAccent,
          onPressed: () {
            showDialog(
              context: context,
              // barrierDismissible: true, // user must tap button for close dialog!
              builder: (BuildContext context) {
                return AlertDialog(
                  title: Text('Бажаєте додати елемент?'),
                  shape: RoundedRectangleBorder(
                      side:  BorderSide(color: Colors.lightBlue,width: 3),
                      borderRadius: BorderRadius.all(Radius.circular(15))
                  ),
                  actions: <Widget>[
                    ElevatedButton(
                      child: Text('Ні'),
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                    ),
                    ElevatedButton(
                        child: Text('Так'),
                        onPressed: () {
                          showDialog(
                              context: context,
                              builder: (BuildContext context) {
                                return AlertDialog(
                                  title: Text('Додайте елемент'),
                                  content: TextField(
                                    onChanged: (String value) {
                                      _userToDo = value;
                                    },
                                  ),
                                  shape: RoundedRectangleBorder(
                                      side:  BorderSide(color: Colors.lightBlue,width: 3),
                                      borderRadius: BorderRadius.all(Radius.circular(15))
                                  ),
                                  backgroundColor: Colors.white,
                                  actions: [
                                    ElevatedButton(
                                      onPressed: () {
                                        setState(() {
                                          todolist.add(_userToDo);
                                        });
                                        Navigator.of(context).pop();
                                      },
                                      child: Text('Додати', style: TextStyle(
                                          fontFamily: 'Cookie'
                                      ),
                                      ),
                                    )
                                  ],
                                );
                              }
                          );
                        }
                    )
                  ],
                );
              },
            );
          },
          child: Icon(
            Icons.add,
            color: Colors.white,
          )),
    );
  }
}
