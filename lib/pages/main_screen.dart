import 'package:flutter/material.dart';

class MainScreen extends StatelessWidget {
  const MainScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.grey[500],
        appBar: AppBar(
          title: Text('Список справ на сьогодні'),
          centerTitle: true,
        ),
        body:Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Column (
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text ('Main  screen'),
                  ElevatedButton(onPressed: () {
                    Navigator.pushReplacementNamed (context, '/goto');
                  }, child: Text ('Перейти далі')),
                ],
              )
            ]
        )
    );
  }
}
